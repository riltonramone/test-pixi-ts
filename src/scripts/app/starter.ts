import {
    Dom,
    PixiAppWrapper as Wrapper,
    pixiAppWrapperEvent as WrapperEvent,
    PixiAppWrapperOptions as WrapperOpts,
} from "pixi-app-wrapper";
import {TweenLite, Linear} from "gsap";
import {Asset, AssetPriority, LoadAsset, PixiAssetsLoader, SoundAsset} from "pixi-assets-loader";

import {BaseState} from "./states/base-state";
import {MainMenu} from "./states/main-menu";
import {CardsState} from "./states/cards-state";
import {ChatState} from "./states/chat-state";
import {FireState} from "./states/fire-state";

export class Starter {

    private app: Wrapper;
    private screenBorder: PIXI.Graphics;

    private loader: PixiAssetsLoader;
    private totalAssets: number;
    private loadingProgress: number;
    private assetsCount: { [key: number]: { total: number, progress: number } } = {};

    private mainMenu: MainMenu;
    private cardsState: CardsState;
    private chatState: ChatState;
    private fireState: FireState;

    private currentState: BaseState;

    constructor()
    {
        const canvas = Dom.getElementOrCreateNew<HTMLCanvasElement>("app-canvas", "canvas", document.getElementById("app-root"));

        const appOptions: WrapperOpts = {
            width: 720,
            height: 1280,
            scale: "keep-aspect-ratio",
            align: "middle",
            resolution: window.devicePixelRatio,
            roundPixels: true,
            transparent: false,
            backgroundColor: 0xdddddd,
            view: canvas,
            showFPS: true,
            showMediaInfo: false,
            changeOrientation: true,
        };

        this.app = new Wrapper(appOptions);

        const assets = [
            {id: "riffic_free", url: "assets/fonts/riffic_free.fnt", priority: AssetPriority.HIGHEST, type: "font"},
            {id: "button", url: "assets/gfx/button.png", priority: AssetPriority.LOW, type: "texture"},
            {id: "card", url: "assets/gfx/card.png", priority: AssetPriority.LOW, type: "texture"},
            {id: "candle", url: "assets/gfx/candle.png", priority: AssetPriority.LOW, type: "texture"},
            {id: "fire_particle", url: "assets/gfx/fire-particle.png", priority: AssetPriority.LOW, type: "texture"},
            {id: "empty_wide", url: "assets/gfx/empty_image_wide.png", priority: AssetPriority.LOW, type: "texture"},
            {id: "empty_circle", url: "assets/gfx/empty_image_circle.png", priority: AssetPriority.LOW, type: "texture"}
        ];

        assets.forEach(asset =>
        {
           if (!this.assetsCount[asset.priority]) {
               this.assetsCount[asset.priority] = {total: 1, progress: 0};
           } else {
               this.assetsCount[asset.priority].total++;
           }
        });

        this.loadingProgress = 0;
        this.totalAssets = assets.length;

        this.loader = new PixiAssetsLoader();
        this.loader.on(PixiAssetsLoader.ASSET_ERROR, this.onAssetsError.bind(this));
        this.loader.on(PixiAssetsLoader.ALL_ASSETS_LOADED, this.onAllAssetsLoaded.bind(this));

        this.loader.addAssets(assets).load();

        TweenLite.defaultEase = Linear.easeNone;
    }

    private onAssetsError(args: LoadAsset): void
    {
        window.console.log(`[SAMPLE APP] onAssetsError ${args.asset.id}: ${args.error!.message}`);
    }

    private onAllAssetsLoaded(): void
    {
        window.console.log("[SAMPLE APP] onAllAssetsLoaded !!!!");

        this.loadMenuState();
    }

    private loadMenuState()
    {
        this.mainMenu = new MainMenu(this.app);

        this.mainMenu.on("cards_clicked", () =>
        {
            this.mainMenu.unload(this.app);

            this.loadCardsState();
        });
        this.mainMenu.on("chat_clicked", () =>
        {
            this.mainMenu.unload(this.app);

            this.loadChatState();
        });
        this.mainMenu.on("effects_clicked", () =>
        {
            this.mainMenu.unload(this.app);

            this.loadFireState();
        });
    }

    private loadCardsState()
    {
        this.cardsState = new CardsState(this.app);

        this.cardsState.on("back_clicked", () =>
        {            
            this.cardsState.unload(this.app);

            this.loadMenuState();
        });
    }

    private loadChatState()
    {
        this.chatState = new ChatState(this.app);

        this.chatState.on("back_clicked", () =>
        {            
            this.chatState.unload(this.app);

            this.loadMenuState();
        });
    }

    private loadFireState()
    {
        this.fireState = new FireState(this.app);

        this.fireState.on("back_clicked", () =>
        {            
            this.fireState.unload(this.app);

            this.loadMenuState();
        });
    }
}
