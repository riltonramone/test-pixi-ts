import {
    PixiAppWrapper as Wrapper
} from "pixi-app-wrapper";

import EventEmitter = require("eventemitter3");

export class BaseState
{
    protected mainContainer: PIXI.Container;
    protected eventEmitter = new EventEmitter();

    constructor(app : Wrapper)
    {
        this.mainContainer = new PIXI.Container();
        this.eventEmitter = new EventEmitter();

        app.stage.addChild(this.mainContainer);   
    }

    public on(eventName: string, listener: EventEmitter.ListenerFn)
    {
       this.eventEmitter.on(eventName, listener); 
    }

    public unload(app : Wrapper)
    {
       app.stage.removeChild(this.mainContainer);
    }
}
