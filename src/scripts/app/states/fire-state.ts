import {
    PixiAppWrapper as Wrapper
} from "pixi-app-wrapper";

import {BaseState} from "./base-state";
import Particles = require("pixi-particles");

export class FireState extends BaseState
{
    private bitmapTextStyle: PIXI.extras.BitmapTextStyle = {font: "48px riffic_free", align: "center"};
    private fireContainer: PIXI.Container;

    constructor(app : Wrapper)
    {
        super(app);

        /*
        * BACK BUTTON
        */
        const button = new PIXI.Container();

        const background = new PIXI.Sprite(PIXI.loader.resources.button.texture);
        background.interactive = true;
        background.buttonMode = true;
        background.pivot.set(0.5);
        background.on("pointerup", () =>
        {
            this.eventEmitter.emit("back_clicked");
        });

        const label = new PIXI.extras.BitmapText("BACK", this.bitmapTextStyle);
        label.tint = 0x444444;
        label.pivot.set(0.5);
        label.position.set(background.width * 0.5 - label.width * 0.5, background.height * 0.5 - label.height * 0.5 - 10);

        button.addChild(background);
        button.addChild(label);

        button.x = app.initialWidth * 0.5 - PIXI.loader.resources.button.texture.width * 0.5;
        button.y = app.initialHeight - PIXI.loader.resources.button.texture.height - 10;

        this.mainContainer.addChild(button);

        /*
        * CANDLE IN THE WIND
        */

        const candle = new PIXI.Sprite(PIXI.loader.resources.candle.texture);
        candle.pivot.set(0.5, 1);
        candle.position.set(app.initialWidth * 0.5 - candle.width * 0.5, app.initialHeight * 0.5);
        this.mainContainer.addChild(candle);

        this.fireContainer  = new PIXI.Container();
        this.fireContainer.position.set(app.initialWidth * 0.5, app.initialHeight * 0.5 + 10);

        this.mainContainer.addChild(this.fireContainer);

        this.showFire();
    }

    showFire(): void
    {
        var emitter = new Particles.Emitter(
            this.fireContainer,
            [ PIXI.loader.resources.fire_particle.texture],
            {
                "alpha": {
                    "start": 0.62,
                    "end": 0
                },
                "scale": {
                    "start": 0.45,
                    "end": 0.95
                },
                "color": {
                    "list": [
                        {"value":"fff191", "time":0},
                        {"value":"ff622c", "time":1}
                    ],
                    "isStepped": false
                },
                "speed": {
                    "start": 150,
                    "end": 150
                },
                "startRotation": {
                    "min": 265,
                    "max": 275
                },
                "rotationSpeed": {
                    "min": 50,
                    "max": 50
                },
                "lifetime": {
                    "min": 0.1,
                    "max": 0.5
                },
                "blendMode": "normal",
                "frequency": 0.001,
                "emitterLifetime": 0,
                "maxParticles": 10,
                "pos": {
                    "x": 0,
                    "y": 0
                },
                "addAtBack": false,
                "spawnType": "circle",
                "spawnCircle": {
                    "x": 0,
                    "y": 0,
                    "r": 10
                }
            }
        );

        var elapsed = Date.now();
                
        var update = function()
        {
            requestAnimationFrame(update);

            var now = Date.now();

            emitter.update((now - elapsed) * 0.001);
            elapsed = now;
        };

        emitter.emit = true;

        update();
    }
}
