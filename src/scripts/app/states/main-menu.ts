import {
    PixiAppWrapper as Wrapper
} from "pixi-app-wrapper";
import {BaseState} from "./base-state";

export class MainMenu extends BaseState
{
    private bitmapTextStyle: PIXI.extras.BitmapTextStyle = {font: "48px riffic_free", align: "center"};

    private menuItems: string[] = ["CARDS", "CHAT", "EFFECTS"];

    constructor(app : Wrapper)
    {
        super(app);

        const buttonsContainer = new PIXI.Container();

        for (var i = this.menuItems.length - 1; i >= 0; i--)
        {
            const button = new PIXI.Container();

            const buttonText = this.menuItems[i];

            const background = new PIXI.Sprite(PIXI.loader.resources.button.texture);
            background.interactive = true;
            background.buttonMode = true;
            background.pivot.set(0.5);
            background.on("pointerup", () =>
            {
                this.eventEmitter.emit(buttonText.toLowerCase()+"_clicked");
            });

            const label = new PIXI.extras.BitmapText(buttonText, this.bitmapTextStyle);
            label.tint = 0x444444;
            label.pivot.set(0.5);
            label.position.set(background.width * 0.5 - label.width * 0.5, background.height * 0.5 - label.height * 0.5 - 10);

            button.addChild(background);
            button.addChild(label);

            button.y = i * (button.height + 50);

            buttonsContainer.addChild(button);
        }

        buttonsContainer.x = app.initialWidth * 0.5 - buttonsContainer.width * 0.5;
        buttonsContainer.y = app.initialHeight * 0.5 - buttonsContainer.height * 0.5;
        
        this.mainContainer.addChild(buttonsContainer);   
    }
}
