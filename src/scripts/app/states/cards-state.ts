import {TweenLite, Linear} from "gsap";
import {
    PixiAppWrapper as Wrapper
} from "pixi-app-wrapper";
import {BaseState} from "./base-state";

export class CardsState extends BaseState
{
    private bitmapTextStyle: PIXI.extras.BitmapTextStyle = {font: "48px riffic_free", align: "center"};
    private numOfCards: number = 144;
    private movedCards: number = 0;
    private intervalId: number;
    private movementSpeed: number = 2;
    private frequencySpeed: number = 1000;

    private cardsContainerTop: PIXI.Container = new PIXI.Container();
    private cardsContaineDown: PIXI.Container = new PIXI.Container();
    private isTopDown: boolean;

    private stackContainers: PIXI.Container[];
    private stackIndex: number = 1;

    constructor(app : Wrapper)
    {
        super(app);
        
        /*
        * BACK BUTTON
        */
        const button = new PIXI.Container();

        const background = new PIXI.Sprite(PIXI.loader.resources.button.texture);
        background.interactive = true;
        background.buttonMode = true;
        background.pivot.set(0.5);
        background.on("pointerup", () =>
        {
            this.eventEmitter.emit("back_clicked");
        });

        const label = new PIXI.extras.BitmapText("BACK", this.bitmapTextStyle);
        label.tint = 0x444444;
        label.pivot.set(0.5);
        label.position.set(background.width * 0.5 - label.width * 0.5, background.height * 0.5 - label.height * 0.5 - 10);

        button.addChild(background);
        button.addChild(label);

        button.x = app.initialWidth * 0.5 - PIXI.loader.resources.button.texture.width * 0.5;
        button.y = app.initialHeight - PIXI.loader.resources.button.texture.height - 10;

        this.mainContainer.addChild(button);

        /*
        * CARD CONTAINERS
        */

        this.stackContainers = [this.cardsContainerTop, this.cardsContaineDown];

        this.cardsContainerTop.x = app.initialWidth * 0.5 - PIXI.loader.resources.card.texture.width * 0.5;
        this.cardsContainerTop.y = this.numOfCards + 10;

        this.cardsContaineDown.x = app.initialWidth * 0.5 - PIXI.loader.resources.card.texture.width * 0.5;
        this.cardsContaineDown.y = app.initialHeight - PIXI.loader.resources.card.texture.height - 20 - button.height;

        for (var i = 0; i < this.numOfCards; i++)
        {
            const card = new PIXI.Sprite(PIXI.loader.resources.card.texture);
            card.interactive = false;
            card.pivot.set(0.5);
            card.position.set(0, -i);
            this.cardsContainerTop.addChild(card);
        }

        this.mainContainer.addChild(this.cardsContainerTop);
        this.mainContainer.addChild(this.cardsContaineDown);

        this.isTopDown = true;

        /*
        * START MOVING
        */

        this.moveStack(this.cardsContainerTop, this.cardsContaineDown);
    }

    moveStack(from: PIXI.Container, to: PIXI.Container):void
    {

        this.intervalId = window.setInterval( () =>
        {
            if(this.movedCards < this.numOfCards)
            {
                let c = from.getChildAt(from.children.length-1);

                c.setParent(this.mainContainer);
                c.position.set(from.x, from.y - (this.numOfCards - this.movedCards));

                let yPos = this.movedCards;

                TweenLite.to(c, this.movementSpeed, {
                    x: to.x,
                    y: to.y - yPos,
                    ease: Linear.easeOut,
                    onComplete: () =>
                    {
                        c.setParent(to);
                        c.position.set(0, -yPos);
                    }
                });
                
                this.movedCards++;
            }
            else
            {
                window.clearInterval(this.intervalId);

                this.movedCards = 0;

                this.isTopDown = !this.isTopDown;

                this.moveStack(this.isTopDown ? this.cardsContainerTop : this.cardsContaineDown, this.isTopDown ? this.cardsContaineDown : this.cardsContainerTop);
            }

        }, this.frequencySpeed);

    }
}
