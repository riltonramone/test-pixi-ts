import {TweenLite} from "gsap";
import {
    PixiAppWrapper as Wrapper
} from "pixi-app-wrapper";

import {BaseState} from "./base-state";

class ChatPattern
{
    public content:number[];

    constructor(content:number[])
    {
        this.content = content;
    }
}

export class ChatState extends BaseState
{
    private static TYPE_IMAGE_1: number = 1;
    private static TYPE_IMAGE_2: number = 2;
    private static TYPE_TEXT: number = 3;

    private patterns: ChatPattern[] = [
        new ChatPattern([ChatState.TYPE_IMAGE_1, ChatState.TYPE_TEXT, ChatState.TYPE_IMAGE_2]),
        new ChatPattern([ChatState.TYPE_IMAGE_1, ChatState.TYPE_IMAGE_2, ChatState.TYPE_IMAGE_1]),
        new ChatPattern([ChatState.TYPE_IMAGE_2, ChatState.TYPE_IMAGE_1, ChatState.TYPE_TEXT]),
        new ChatPattern([ChatState.TYPE_TEXT, ChatState.TYPE_IMAGE_1, ChatState.TYPE_TEXT])
    ]

    private bitmapTextStyle1: PIXI.extras.BitmapTextStyle = {font: "48px riffic_free", align: "left"};
    private bitmapTextStyle2: PIXI.extras.BitmapTextStyle = {font: "32px riffic_free", align: "left"};
    private bitmapTextStyle3: PIXI.extras.BitmapTextStyle = {font: "24px riffic_free", align: "left"};
    private textStyles: PIXI.extras.BitmapTextStyle[] = [
        {font: "48px riffic_free", align: "left"},
        {font: "32px riffic_free", align: "left"},
        {font: "24px riffic_free", align: "left"}
    ];

    private chatContainer: PIXI.Container;
    private intervalId: number;
    private movementSpeed: number = 1;
    private frequencySpeed: number = 2000;
    private lastAdded:PIXI.Container;

    constructor(app : Wrapper)
    {
        super(app);

        /*
        * CHAT
        */

        this.chatContainer = new PIXI.Container();

        this.intervalId = window.setInterval( () =>
        {
            let rand = Math.floor(Math.random() * this.patterns.length);
            this.addLine(this.patterns[rand]);

            if(this.lastAdded.y + this.lastAdded.height > app.initialHeight)
            {
                let yPos = app.initialHeight - (this.lastAdded.y + this.lastAdded.height);

                TweenLite.to(this.chatContainer, this.movementSpeed, {
                    y: yPos
                });
            }

            /*
            * CLEANING
            */

            if(this.chatContainer.height > app.initialHeight * 1.5)
            {
                this.chatContainer.removeChildAt(0);
            }

        }, this.frequencySpeed);

        let rand = Math.floor(Math.random() * this.patterns.length);
        this.addLine(this.patterns[rand]);

        this.mainContainer.addChild(this.chatContainer);

        /*
        * BACK BUTTON
        */
        const button = new PIXI.Container();

        const background = new PIXI.Sprite(PIXI.loader.resources.button.texture);
        background.interactive = true;
        background.buttonMode = true;
        background.pivot.set(0.5);
        background.on("pointerup", () =>
        {
            this.eventEmitter.emit("back_clicked");
        });

        const label = new PIXI.extras.BitmapText("BACK", this.bitmapTextStyle1);
        label.tint = 0x444444;
        label.pivot.set(0.5);
        label.position.set(background.width * 0.5 - label.width * 0.5, background.height * 0.5 - label.height * 0.5 - 10);

        button.addChild(background);
        button.addChild(label);

        button.x = app.initialWidth * 0.5 - PIXI.loader.resources.button.texture.width * 0.5;
        button.y = app.initialHeight - PIXI.loader.resources.button.texture.height - 10;

        this.mainContainer.addChild(button);
    }

    private addLine(pattern:ChatPattern)
    {
        const lineContainer = new PIXI.Container();
        lineContainer.y = this.lastAdded != null ? this.lastAdded.y + this.lastAdded.height + 10 : 0;

        for (let i = 0; i < pattern.content.length; i++)
        {    
            switch (pattern.content[i]) {
                case ChatState.TYPE_IMAGE_1:

                    const image_circle = new PIXI.Sprite(PIXI.loader.resources.empty_circle.texture);
                    image_circle.x = i > 0 ? lineContainer.width + 10 : 0;
                    image_circle.tint = Math.random() * 0xdddddd;
                    lineContainer.addChild(image_circle);

                    break;
                case ChatState.TYPE_IMAGE_2:
                    
                    const image_wide = new PIXI.Sprite(PIXI.loader.resources.empty_wide.texture);
                    image_wide.x = i > 0 ? lineContainer.width + 10 : 0;
                    image_wide.tint = Math.random() * 0xdddddd;
                    lineContainer.addChild(image_wide);

                    break;
                case ChatState.TYPE_TEXT:

                    const label = new PIXI.extras.BitmapText((Math.random() * 1000000).toFixed(2), this.textStyles[Math.floor(Math.random() * this.textStyles.length)]);
                    // label.pivot.set(0, 0.5);
                    label.tint = Math.random() * 0xdddddd;
                    label.x = i > 0 ? lineContainer.width + 10 : 0;
                    lineContainer.addChild(label);

                    break;
                default:
                    // code...
                    break;
            }
        }

        this.chatContainer.addChild(lineContainer);
        this.lastAdded = lineContainer;
    }
}
